package com.dshatz.bestmusicdownloader;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


/**
 * Created by Danik on 19/05/2015.
 */
public class BeforeUploadDialog extends Dialog {


    public BeforeUploadDialog(Context context) {
        super(context);
    }


    private EditText editTitle, editArtist;
    private Button okButton, cancelButton;

    private View.OnClickListener okListener, cancelListener;

    private String title,artist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.dialog_before_upload);


        setTitle(R.string.confirm);

        editTitle = (EditText) findViewById(R.id.titleEdit);
        editArtist = (EditText) findViewById(R.id.artistEdit);
        okButton = (Button) findViewById(R.id.okButton);
        cancelButton = (Button) findViewById(R.id.cancelButton);

        if(okListener != null) okButton.setOnClickListener(okListener);
        if(cancelListener != null) cancelButton.setOnClickListener(cancelListener);

        if(title != null) editTitle.setText(title);
        if(artist != null) editArtist.setText(artist);
    }

    public void setData(String title, String artist){
        if(title != null) this.title = title.trim();
        if(title != null) this.artist = artist.trim();
    }


    public void setOkButtonListener(View.OnClickListener l) {
        okListener = l;
        if(okButton != null) okButton.setOnClickListener(okListener);
    }

    public void setCancelButtonListener(View.OnClickListener l){
        cancelListener = l;
        if(cancelButton != null) cancelButton.setOnClickListener(cancelListener);
    }

    public String getTitle(){
        return editTitle.getText().toString();
    }

    public String getArtist(){
        return editArtist.getText().toString();
    }
}
