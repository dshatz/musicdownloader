package com.dshatz.bestmusicdownloader;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Danik on 20/05/2015.
 */
public class SearchResultAdapter extends ArrayAdapter<SearchResult> {

    public SearchResultAdapter(Context context, ArrayList<SearchResult> users) {
        super(context, 0, users);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        SearchResult r = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.song_layout, parent, false);
        }

        TextView titleView = (TextView) convertView.findViewById(R.id.titleView);
        TextView artistView = (TextView) convertView.findViewById(R.id.artistView);
        TextView durationRawView = (TextView) convertView.findViewById(R.id.durationRawView);
        TextView durationView = (TextView) convertView.findViewById(R.id.durationView);
        TextView bitrateView = (TextView) convertView.findViewById(R.id.bitrateView);


        int min = (int)(r.duration / 60f);
        int s = r.duration - min * 60;

        titleView.setText(r.title);
        artistView.setText(r.artist);
        durationRawView.setText(r.duration);
        durationView.setText(min + ((s < 10) ? ":0" : ":") + s);


        return convertView;
    }
}