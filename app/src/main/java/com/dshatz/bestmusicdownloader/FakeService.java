package com.dshatz.bestmusicdownloader;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;

public class FakeService extends Service {

    public static int notif_id = 230;


    public FakeService() {
    }


    public static FakeService instance;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        instance = this;

        startForeground(notif_id, new NotificationCompat.Builder(getApplicationContext()).setSmallIcon(R.drawable.logo).setContentIntent(PendingIntent.getActivity(getApplicationContext(), 0, new Intent(), 0)).build());
        CaptchaService.instance.runStartForeground();
        return super.onStartCommand(intent, flags, startId);
    }


    @Override
    public void onDestroy() {
        stopForeground(true);
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
