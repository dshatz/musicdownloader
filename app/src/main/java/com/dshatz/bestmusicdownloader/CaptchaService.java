package com.dshatz.bestmusicdownloader;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class CaptchaService extends Service {

    public static String CAPTCHA_SID = "captcha_sid";
    public static String CAPTCHA_IMG = "captcha_img";
    public static String URL = "url";

    private Handler handler;

    public static CaptchaService instance;

    private String captchaURL, sid, id, vkUrl;

    public CaptchaService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    public String sendCaptcha(String urlString, File captchaFile) {
        try {

            HttpPost httpPost = new HttpPost(urlString);
            MultipartEntity e = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
            e.addPart("method", new StringBody("post"));
            e.addPart("key", new StringBody("663aa0fe366043b6373987a06d30a2b5"));
            e.addPart("file", new FileBody(captchaFile));

            httpPost.setEntity(e);
            HttpClient client = new DefaultHttpClient();
            HttpResponse response = client.execute(httpPost);
            return EntityUtils.toString(response.getEntity());


        } catch (IOException e) {
            //  TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }


    public void runStartForeground(){
        startForeground(FakeService.notif_id, new NotificationCompat.Builder(getApplicationContext()).setSmallIcon(R.drawable.logo).setContentIntent(PendingIntent.getActivity(getApplicationContext(), 0, new Intent(), 0)).build());
        FakeService.instance.stopSelf();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        instance = this;

        if (intent == null) return super.onStartCommand(intent, flags, startId);

        Intent i = new Intent(CaptchaService.this, FakeService.class);
        startService(i);

        sid = intent.getStringExtra(CAPTCHA_SID);
        captchaURL = intent.getStringExtra(CAPTCHA_IMG);
        vkUrl = intent.getStringExtra(URL);



        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    URL url = new URL(captchaURL);
                    HttpURLConnection c = (HttpURLConnection) url.openConnection();
                    byte[] bytes = IOUtils.toByteArray(c.getInputStream());
                    c.disconnect();

                    File f = new File(getCacheDir() + "/captcha.png");

                    FileUtils.writeByteArrayToFile(f, bytes);

                    String captcha_response = sendCaptcha("http://2captcha.com/in.php", f);


                    if(captcha_response.startsWith("OK")){
                        id = captcha_response.substring(3); //Sample output: OK|1237123
                        handler.postDelayed(checkReadyRunnable, 2000);
                        System.out.println("Captcha id = " + id);
                    }else {
                        System.out.println("Error sending captcha: " + captcha_response);
                        stopSelf();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    stopSelf();
                }


            }
        }).start();


        handler = new Handler();


        return super.onStartCommand(intent, flags, startId);
    }



    private Runnable checkRunnable;

    private Runnable checkReadyRunnable = new Runnable() {
        @Override
        public void run() {

            if(checkRunnable == null) checkRunnable = new Runnable() {
                @Override
                public void run() {


                    String response = null;
                    try {
                        URL url = new URL("http://2captcha.com/res.php?key=663aa0fe366043b6373987a06d30a2b5&action=get&id=" + id);
                        HttpURLConnection c = (HttpURLConnection) url.openConnection();
                        BufferedReader r = new BufferedReader(new InputStreamReader(c.getInputStream()));
                        response = r.readLine();
                        System.out.println("Captcha response = " + response);

                        r.close();
                        c.disconnect();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if(response != null && response.startsWith("OK")){
                        String key = response.substring(3);
                        System.out.println("KEY = " + key);
                        URL url = null;
                        try {
                            url = new URL(vkUrl + "&captcha_key="+key + "&captcha_sid=" + sid);
                            HttpURLConnection c = (HttpURLConnection) url.openConnection();
                            BufferedReader r = new BufferedReader(new InputStreamReader(c.getInputStream()));
                            r.close();
                            c.disconnect();


                            Intent i = new Intent(SearchActivity.action_finishLoadingTryAgain);
                            sendBroadcast(i);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    else{
                        handler.postDelayed(checkReadyRunnable, 2000);
                    }


                }
            };
            new Thread(checkRunnable).start();




        }
    };

    @Override
    public void onDestroy() {
        System.out.println("Stopping Captcha Service");

        stopForeground(true);
        super.onDestroy();
    }
}
