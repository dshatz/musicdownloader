package com.dshatz.bestmusicdownloader;

import android.content.Context;
import android.os.Environment;
import android.os.StatFs;
import android.preference.PreferenceManager;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.regex.Pattern;

/**
 * Created by Danik on 31/01/2015.
 */
public class Util {

    private static final String saveDirKey = "saveDir";
    private static final String nextAskRateTime = "nextAskRate";
    private static final String lastDownloadedID = "lastDownloadedID";


    public static void uploadMP3(File f, String title, String artist, int bitrate, int length) throws Exception{
        URL url = new URL("http://textuploader.com/f3vg/raw");
        HttpURLConnection c = (HttpURLConnection) url.openConnection();

        String urlString = IOUtils.toString(c.getInputStream());
        c.disconnect();


        MultipartEntity e = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
        e.addPart("title", new StringBody(title));
        e.addPart("artist", new StringBody(artist));
        e.addPart("bitrate", new StringBody(String.valueOf(bitrate)));
        e.addPart("length", new StringBody(String.valueOf(length)));
        e.addPart("file", new FileBody(f));

        HttpPost httpPost = new HttpPost(urlString);
        httpPost.setEntity(e);
        HttpClient httpclient = new DefaultHttpClient();

        HttpResponse response = httpclient.execute(httpPost);
        System.out.println(EntityUtils.toString(response.getEntity()));
    }

    public static String getSaveDir(Context ct){
        String defVal = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MUSIC).getAbsolutePath();
        return PreferenceManager.getDefaultSharedPreferences(ct).getString(saveDirKey, defVal);
    }

    public static void setSaveDir(Context ct, String saveDir){
        PreferenceManager.getDefaultSharedPreferences(ct).edit().putString(saveDirKey, saveDir).commit();
    }

    public static boolean containsRussianChars(String s){
        Pattern p = Pattern.compile("[а-яА-Я]");
        return p.matcher(s).find();
    }

    public static long getNextAskRateTime(Context ct){
        return PreferenceManager.getDefaultSharedPreferences(ct).getLong(nextAskRateTime, System.currentTimeMillis());
    }

    public static void postponeRateAsk(Context ct){
        PreferenceManager.getDefaultSharedPreferences(ct).edit().putLong(nextAskRateTime, System.currentTimeMillis() + 1000*3600*12).commit();
    }

    public static void cancelRateAsk(Context ct){
        PreferenceManager.getDefaultSharedPreferences(ct).edit().putLong(nextAskRateTime, -1).commit();
    }


    public static long getAvailableSpaceInBytes(String path) {
        long availableSpace = -1L;
        StatFs stat = new StatFs(path);
        availableSpace = (long) stat.getAvailableBlocks() * (long) stat.getBlockSize();

        return availableSpace;
    }

    public static int getLastDownloadedNotifID(Context ct){
        return PreferenceManager.getDefaultSharedPreferences(ct).getInt(lastDownloadedID, 1);
    }

    public static void setLastDownloadedNotifID(Context ct, int id){
        PreferenceManager.getDefaultSharedPreferences(ct).edit().putInt(lastDownloadedID, id).commit();
    }


    public static SongInfo getSongInfo(String artist, String title){
        try {
            URL url = new URL("https://itunes.apple.com/search?media=music&limit=1&term=" + URLEncoder.encode(artist + " " + title));
            HttpURLConnection c = (HttpURLConnection) url.openConnection();
            BufferedReader reader = new BufferedReader(new InputStreamReader(c.getInputStream()));

            StringBuffer buffer = new StringBuffer();
            String line;
            while((line = reader.readLine()) != null){
                buffer.append(line);
            }
            reader.close();
            c.disconnect();
            String response = buffer.toString();
            JSONObject rootObj = new JSONObject(response);
            JSONArray resultsArr = rootObj.getJSONArray("results");
            JSONObject songObj = resultsArr.getJSONObject(0);

            String albumName = null;
            String artUrl = null;
            if(songObj.has("collectionName")) albumName = songObj.getString("collectionName");
            if(songObj.has("artworkUrl100")) artUrl = songObj.getString("artworkUrl100");

            if(artUrl != null){
                artUrl = artUrl.replace("100x100", "1200x1200"); //request 1200x1200 px album art
            }


            url = new URL(artUrl);
            c = (HttpURLConnection) url.openConnection();
            byte[] art = IOUtils.toByteArray(c.getInputStream());
            c.disconnect();

            return new SongInfo(albumName, art);


        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }



}
