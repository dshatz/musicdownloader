package com.dshatz.bestmusicdownloader;

/**
 * Created by Danik on 07/02/2015.
 */
public class SongDownload  {

    public String title, artist, url;

    public SongDownload(String title, String artist, String url){
        this.title = title;
        this.artist = artist;
        this.url = url;
    }

}
