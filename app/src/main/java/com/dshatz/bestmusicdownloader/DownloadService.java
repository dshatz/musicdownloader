package com.dshatz.bestmusicdownloader;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

import com.mpatric.mp3agic.ID3v2;
import com.mpatric.mp3agic.ID3v24Tag;
import com.mpatric.mp3agic.Mp3File;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Danik on 07/02/2015.
 */
public class DownloadService extends Service {
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    private boolean downloading = false;

    private String currUrl, currTitle, currArtist;

    private List<SongDownload> queue;

    public static final String titleExtra = "title";
    public static final String urlExtra = "url";
    public static final String artistExtra = "artist";

    private NotificationManager notifManager;

    private NotificationCompat.Builder ongoingNotifBuilder;
    private NotificationCompat.Builder downloadingNotifBuilder;
    private NotificationCompat.Builder downloadedNotifBuilder;

    private Runnable insufficientMemory = new Runnable() {
        @Override
        public void run() {
            Toast.makeText(getBaseContext(), R.string.nospace, Toast.LENGTH_SHORT).show();
        }
    };



    private final int ongoingId = 999;

    private final int downloadingNotifId = 499;

    private int downloadedNotifId = 1;

    private void showDownloadedNotification(String title, boolean failed){
        downloadedNotifBuilder.setContentTitle(title);

        if(failed) downloadedNotifBuilder.setContentText(getResources().getString(R.string.download_failed));
        else downloadedNotifBuilder.setContentText(getResources().getString(R.string.download_complete));

        notifManager.notify(downloadedNotifId, downloadedNotifBuilder.build());
        downloadedNotifId ++;
    }

    /**
     * @param progress set to negative to switch to indeterminate mode
     * @param title
     */
    private void showDownloadingNotification(int progress, String title){
        downloadingNotifBuilder.setContentTitle(title);
        downloadingNotifBuilder.setProgress(100, progress, (progress < 0));
        notifManager.notify(downloadingNotifId, downloadingNotifBuilder.build());
    }

    private void cancelDownloadingNotification(){
        notifManager.cancel(downloadingNotifId);
    }

    private void downloadNext(){
        if(queue.size() > 0){
            currTitle = queue.get(0).title;
            currArtist = queue.get(0).artist;
            currUrl = queue.get(0).url;
            queue.remove(0);
            new Thread(download).start();
        }
        else{
            stopSelf();
        }
    }


    public static final String action_cancel = "cancel_download";
    private BroadcastReceiver receiver;

    public void onCreate() {
        super.onCreate();
        System.out.println("onCreate");

        IntentFilter filter = new IntentFilter();
        filter.addAction(action_cancel);
        // Add other actions as needed

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(action_cancel)) {
                    cancelDownloadingNotification();
                    cancelled = true;
                }
            }
        };

        registerReceiver(receiver, filter);


        queue = new ArrayList<SongDownload>();

        downloadedNotifId = Util.getLastDownloadedNotifID(this);
        notifManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        ongoingNotifBuilder = new NotificationCompat.Builder(this).setOngoing(true).setContentTitle(getResources().getString(R.string.downloading)).setSmallIcon(R.drawable.icon_white).setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.icon_white));
        downloadedNotifBuilder = new NotificationCompat.Builder(this).setOngoing(false).setSmallIcon(android.R.drawable.stat_sys_download_done).setContentText(getResources().getString(R.string.download_complete)).setLargeIcon(BitmapFactory.decodeResource(getResources(), android.R.drawable.stat_sys_download_done));
        downloadingNotifBuilder = new NotificationCompat.Builder(this).setOngoing(true).setSmallIcon(android.R.drawable.stat_sys_download).setLargeIcon(BitmapFactory.decodeResource(getResources(), android.R.drawable.stat_sys_download));


        Intent cancelIntent = new Intent(action_cancel);
        PendingIntent cancelPendingIntent = PendingIntent.getBroadcast(this, 0, cancelIntent, 0);

        downloadingNotifBuilder.addAction(android.R.drawable.ic_menu_close_clear_cancel, getResources().getString(android.R.string.cancel), cancelPendingIntent);

        ongoingNotifBuilder.setContentIntent(PendingIntent.getActivity(getApplicationContext(), 0, new Intent(), 0));
        downloadingNotifBuilder.setContentIntent(PendingIntent.getActivity(getApplicationContext(), 0, new Intent(), 0));
        downloadedNotifBuilder.setContentIntent(PendingIntent.getActivity(getApplicationContext(), 0, new Intent(), 0));


        startForeground(ongoingId, ongoingNotifBuilder.build());
    }

    private void updateOngoingNotification(){
        ongoingNotifBuilder.setContentText(queue.size() + " " + getResources().getString(R.string.inQueue));
        if(queue.size() == 0) ongoingNotifBuilder.setContentText("");
        startForeground(ongoingId, ongoingNotifBuilder.build());
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        System.out.println("onStartCommand");

        if(intent ==  null) return super.onStartCommand(intent, flags, startId);

        String title,artist,url;
        url = intent.getStringExtra(urlExtra);
        title = intent.getStringExtra(titleExtra).trim();
        artist = intent.getStringExtra(artistExtra).trim();



        if(!downloading){
            currUrl = url;
            currTitle = title;
            currArtist = artist;
            new Thread(download).start();
        }else{
            queue.add(new SongDownload(title, artist, url));
            updateOngoingNotification();
        }




        return super.onStartCommand(intent, flags, startId);
    }


    Runnable showSaveLocationUnaccessible = new Runnable() {
        @Override
        public void run() {
            Toast.makeText(DownloadService.this, R.string.saveLocUnaccessible, Toast.LENGTH_LONG).show();
        }
    };

    private boolean cancelled = false;
    private boolean failed = false;


    Runnable download = new Runnable() {
        @Override
        public void run() {
            downloading = true;

            updateOngoingNotification();

            System.out.println("Starting download task for " + currTitle);

            showDownloadingNotification(0, currTitle);

            try {


                File dest = new File(Util.getSaveDir(getBaseContext()) + "/" + currTitle + ".mp");
                File folder = new File(Util.getSaveDir(getBaseContext()));

                try{
                    if(!folder.isDirectory()) folder.mkdirs();
                    if(!dest.exists()) dest.createNewFile();
                } catch(IOException ioe){
                    SearchActivity.instance.runOnUiThread(showSaveLocationUnaccessible);
                    cancelDownloadingNotification();
                    downloading = false;
                    stopForeground(true);
                    return;
                }

                System.out.println("Downloading from: " + currUrl);

                URL adress = new URL(currUrl);
                HttpURLConnection con = (HttpURLConnection) adress.openConnection();


                InputStream in = con.getInputStream();
                int contentLength = con.getContentLength();



                if(contentLength > Util.getAvailableSpaceInBytes(dest.getAbsolutePath())) {
                    SearchActivity.instance.runOnUiThread(insufficientMemory);
                }


                showDownloadingNotification(0, currTitle);


                FileOutputStream out = null;
                if (contentLength != -1) {
                    out = new FileOutputStream(dest);
                }

                byte[] buf = new byte[512];

                int readed = 0;


                int lastPercentage = 0;

                while (true) {
                    if(cancelled) break;

                    int len = 0;
                    try{
                        len = in.read(buf);
                    } catch(Exception e){
                        failed = true;
                        break;
                    }
                    if (len == -1) {
                        break;
                    }
                    out.write(buf, 0, len);
                    readed += len;

                    int percentage = (int)((float)readed/contentLength * 100);

                    if(percentage - lastPercentage > 5){
                        System.out.println(percentage + "% complete: " + currTitle);
                        showDownloadingNotification(percentage, currTitle);
                        lastPercentage = percentage;
                    }


                }
                out.close();



                if(!cancelled){
                    System.out.println("Changing MP3 Headers...");

                    showDownloadingNotification(-1, currTitle);

                    Mp3File mp3file = new Mp3File(dest.getAbsolutePath());


                    String fixedArtist = currArtist;
                    String fixedTitle = currTitle;

                    Pattern p = Pattern.compile("(\\b(?:feat|ft|featuring)\\.{0,1}.+)");


                    String artistFeat = null, titleFeat = null;

                    Matcher artistMatcher = p.matcher(fixedArtist);
                    if(artistMatcher.find()){
                        artistFeat = artistMatcher.group();
                    }
                    Matcher titleMatcher = p.matcher(fixedTitle);
                    if(titleMatcher.find()){
                        titleFeat = artistMatcher.group();
                    }



                    if(artistFeat != null && titleFeat == null){ //Move feat. from song artist to title
                        currArtist = currArtist.replace(artistFeat, ""); //Remove feat from artist

                        currTitle = currTitle.trim() + " " + artistFeat.trim(); //Move feat to title
                    }

                    currArtist = currArtist.replaceAll("(\\(.*\\))", ""); //Remove all braces with content from artist and title
                    currTitle = currTitle.replaceAll("(\\(.*\\))", "");

                    currArtist = currArtist.trim();
                    currTitle = currTitle.trim();

                    fixedArtist = fixedArtist.replaceAll("(\\b(?:feat|ft|featuring)\\.{0,1}.+)", "");
                    fixedTitle = fixedTitle.replaceAll("(\\b(?:feat|ft|featuring)\\.{0,1}.+)", "");
                    fixedTitle = fixedTitle.replaceAll("(\\(.*\\))", "");

                    fixedArtist = fixedArtist.trim();
                    fixedTitle = fixedTitle.trim();


                    ID3v2 id3v2Tag;
                    if (mp3file.hasId3v2Tag()) {
                        id3v2Tag = mp3file.getId3v2Tag();
                    } else {
                        // mp3 does not have an ID3v2 tag, let's create one..
                        id3v2Tag = new ID3v24Tag();
                        mp3file.setId3v2Tag(id3v2Tag);
                    }


                    id3v2Tag.setArtist(currArtist);
                    id3v2Tag.setTitle(currTitle);


                    SongInfo info = Util.getSongInfo(fixedArtist, fixedTitle);
                    if(info != null) {
                        if(info.album != null) id3v2Tag.setAlbum((info.album != null) ? info.album : "Unknown");
                        if(info.albumArt != null) id3v2Tag.setAlbumImage(info.albumArt, "image/jpeg");
                    }

                    if (mp3file.hasId3v1Tag()) {
                        mp3file.removeId3v1Tag();
                    }
                    if (mp3file.hasCustomTag()) {
                        mp3file.removeCustomTag();
                    }


                    File newFile = new File(dest.getAbsolutePath() + "3"); //add 3 to *.mp
                    if(!newFile.isFile()){
                        newFile.createNewFile();
                    }


                    mp3file.save(newFile.getAbsolutePath());

                    System.out.println("Title: " + currTitle);
                    System.out.println("Artist: " + currArtist);

                    System.out.println("MP3 Headers Changed");


                    dest.delete();

                    cancelDownloadingNotification();


                    sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(newFile)));
                }
                else{
                    dest.delete();
                    System.out.println("Download cancelled! Deleting file!");
                }




            } catch (Exception e) {
                e.printStackTrace();
            }


            if(!cancelled) {
                showDownloadedNotification(currTitle, failed);
                System.out.println("Download finished (" + currTitle + ")");
            } else {
                cancelDownloadingNotification();
            }

            cancelled = false;
            downloading =false;
            downloadNext();

        }
    };


    @Override
    public void onDestroy() {
        System.out.println("onDestroy");
        stopForeground(true);
        unregisterReceiver(receiver);
        Util.setLastDownloadedNotifID(this, downloadedNotifId);
        super.onDestroy();
    }




}
