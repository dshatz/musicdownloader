package com.dshatz.bestmusicdownloader;

/**
 * Created by Danik on 2015.05.10..
 */
public class SongInfo {

    public String album;
    public byte[] albumArt;

    public SongInfo(String album, byte[] albumArt){
        this.album = album;
        this.albumArt = albumArt;
    }

}
