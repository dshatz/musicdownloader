package com.dshatz.bestmusicdownloader;

import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;


public class Mp3SelectActivity extends ActionBarActivity implements AdapterView.OnItemClickListener {


    private ListView mp3List;
    private ArrayList<MP3> list;

    public static final String PATH = "path";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mp3_select);

        mp3List = (ListView) findViewById(R.id.mp3List);

        mp3List.setOnItemClickListener(this);
        list = getSongs();
        MP3Adapter adapter = new MP3Adapter(this, list);


        mp3List.setAdapter(adapter);


    }

    private void hideNotContaining(String q){
        int childcount = mp3List.getChildCount();
        for (int i=0; i < childcount; i++){
            View v = mp3List.getChildAt(i);
            String title = ((TextView) findViewById(R.id.title)).getText().toString();
            String artist = ((TextView) findViewById(R.id.artist)).getText().toString();
            String path = ((TextView) findViewById(R.id.path)).getText().toString();

            boolean contains = StringUtils.containsIgnoreCase(title, q) || StringUtils.containsIgnoreCase(artist, q) || StringUtils.containsIgnoreCase(path, q);

            if(contains) v.setVisibility(View.VISIBLE);
            else v.setVisibility(View.GONE);

        }
    }


    public ArrayList<MP3> getSongs(){

        ContentResolver cr = getContentResolver();
        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String selection = MediaStore.Audio.Media.IS_MUSIC + "!= 0";
        String sortOrder = MediaStore.Audio.Media.TITLE + " ASC";
        Cursor cur = cr.query(uri, null, selection, null, sortOrder);

        int count = 0;

        ArrayList<MP3> mList = new ArrayList<MP3>();


        if(cur != null){
            count = cur.getCount();
            if(count > 0){
                while(cur.moveToNext()){

                    String path = cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.DATA));
                    String title = cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.TITLE));
                    String artist = cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.ARTIST));



                    mList.add(new MP3(title, artist, path));
                }
            }
        }
        cur.close();
        return mList;
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_mp3_select, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        MP3 mp3 = list.get(position);

        Intent i = new Intent();
        i.putExtra(PATH, mp3.path);

        setResult(RESULT_OK, i);
        finish();

    }

}
