package com.dshatz.bestmusicdownloader;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.mpatric.mp3agic.Mp3File;

import net.rdrei.android.dirchooser.DirectoryChooserFragment;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.annotation.Nonnull;


public class SearchActivity extends ActionBarActivity implements View.OnClickListener, DirectoryChooserFragment.OnFragmentInteractionListener, MediaPlayer.OnBufferingUpdateListener, MediaPlayer.OnCompletionListener, SeekBar.OnSeekBarChangeListener {


    private EditText searchField;
    private ImageButton searchButton;
    private LinearLayout resultView;
    private TextView captchaErrorView;
    private List<JSONObject> songs;
    private TextView noResultsView;
    private ScrollView scrollView;


    private long lastReportTime = 0;


    private ProgressBar progressBar;


    public static int downloadedID = 1;

    private LayoutInflater inflater;

    private DirectoryChooserFragment dirChooserFragment;


    private boolean playerInitialized = false;

    private AdView adView;


    private ImageButton reportButton;


    private void recreateDialogLayout(){
        seekBar = new SeekBar(this);
        seekBar.setOnSeekBarChangeListener(this);
        dialogLayout = new LinearLayout(this);
        dialogLayout.setOrientation(LinearLayout.VERTICAL);
        bitRateView = new TextView(this);
        bitRateLoadingText = new TextView(this);
        bitRateLoadingText.setText(R.string.loading_bitrate);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.gravity = Gravity.CENTER_HORIZONTAL;

        dialogLayout.addView(seekBar);
        dialogLayout.addView(bitRateLoadingText, params);
        dialogLayout.addView(bitRateView, params);

        bitRateLoadingText.setVisibility(View.VISIBLE);
        bitRateView.setVisibility(View.GONE);
    }


    @Override
    public void onBackPressed() {
        if(true || System.currentTimeMillis() < Util.getNextAskRateTime(getBaseContext())) {
            super.onBackPressed();
            return;
        }

        new AlertDialog.Builder(this).setTitle(R.string.ratePlease).setPositiveButton(R.string.rate, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName)));
                }
                Util.postponeRateAsk(getBaseContext());
                SearchActivity.super.onBackPressed();

            }
        }).setNeutralButton(R.string.later, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Util.postponeRateAsk(getBaseContext());
                SearchActivity.super.onBackPressed();
            }
        }).show();
    }

    @Override
    protected void onDestroy() {
        downloadedID = 1;
        unregisterReceiver(receiver);
        super.onDestroy();
    }


    public static SearchActivity instance;

    private Tracker tracker;

    //private PlusOneButton plusOneButton;
    private BroadcastReceiver receiver;

    public static String action_finishLoadingTryAgain = "action_finishLoadingTryAgain";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        tracker = ((App) getApplication()).getTracker(App.TrackerName.APP_TRACKER);

        tracker.enableAdvertisingIdCollection(true);


        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle(R.string.uploading);
        progressDialog.setMessage(getResources().getString(R.string.please_wait));
        progressDialog.setIndeterminate(true);
        progressDialog.setCanceledOnTouchOutside(false);

        //plusOneButton =(PlusOneButton) findViewById(R.id.plusOneButton);

        IntentFilter filter = new IntentFilter();
        filter.addAction(action_finishLoadingTryAgain);

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if(intent.getAction().equals(action_finishLoadingTryAgain)){
                    progressBar.setVisibility(View.GONE);
                    captchaErrorView.setVisibility(View.GONE);
                    searchButton.setEnabled(true);
                    Toast.makeText(SearchActivity.this, R.string.try_again, Toast.LENGTH_SHORT).show();
                }
            }
        };

        registerReceiver(receiver, filter);


        instance = this;

        songs = new ArrayList<JSONObject>();

        dirChooserFragment = DirectoryChooserFragment.newInstance(getResources().getString(R.string.folder_name), Environment.getExternalStorageDirectory().getPath());

        inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);


        handler.postDelayed(updateProgress,2000);

        searchField = (EditText) findViewById(R.id.searchField);
        searchButton = (ImageButton) findViewById(R.id.searchButton);
        resultView = (LinearLayout) findViewById(R.id.searchResults);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        noResultsView = (TextView) findViewById(R.id.noresultsview);
        captchaErrorView = (TextView) findViewById(R.id.captchaErrorView);
        scrollView = (ScrollView) findViewById(R.id.scrollView);


        searchField.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    listResults();
                    return true;
                }
                return false;
            }
        });


        recreateDialogLayout();



        adView = (AdView) findViewById(R.id.adView);
        //adView.setAdUnitId("ca-app-pub-1827896524149471/2551849346"); Already set in activity_search.xml
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);

        mp = new MediaPlayer();
        mp.setOnBufferingUpdateListener(this);
        mp.setOnCompletionListener(this);


        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listResults();
            }
        });

    }


    private Runnable networkProblemOccurred = new Runnable() {
        @Override
        public void run() {
            Toast.makeText(SearchActivity.this, R.string.networkProblem, Toast.LENGTH_LONG).show();
            searchButton.setEnabled(true);
            progressBar.setVisibility(View.GONE);
            noResultsView.setVisibility(View.GONE);
            progressDialog.dismiss();
        }
    };

    private Runnable parsingErrorOccured = new Runnable() {
        @Override
        public void run() {
            Toast.makeText(SearchActivity.this, R.string.parsingError, Toast.LENGTH_LONG).show();
            searchButton.setEnabled(true);
            progressBar.setVisibility(View.GONE);
            noResultsView.setVisibility(View.GONE);
        }
    };


    private Runnable searchRunnable = new Runnable() {
        @Override
        public void run() {

            long start = System.currentTimeMillis();

            String urlStr = null;
            String upl_urlStr = null;
            try{
                URL addressUrl = new URL("http://textuploader.com/u5an/raw");// Get url
                HttpURLConnection c = (HttpURLConnection) addressUrl.openConnection();
                BufferedReader reader = new BufferedReader(new InputStreamReader(c.getInputStream()));
                urlStr = reader.readLine();
                reader.close();
                c.disconnect();

                addressUrl = new URL("http://textuploader.com/feqd/raw"); //Get uploaded
                c = (HttpURLConnection) addressUrl.openConnection();
                upl_urlStr = IOUtils.toString(c.getInputStream());
                c.disconnect();



                addressUrl = new URL(urlStr);
                c = (HttpURLConnection) addressUrl.openConnection();
                reader = new BufferedReader(new InputStreamReader(c.getInputStream()));
                urlStr = reader.readLine();
                reader.close();
                c.disconnect();
                System.out.println("Searching via " + urlStr);

            } catch (Exception e) {
                e.printStackTrace();
                runOnUiThread(networkProblemOccurred);
                return;
            }




            String q = searchField.getText().toString();
            URL url = null;
            try{
                url = new URL(urlStr + URLEncoder.encode(q));
                System.out.println("Searching on :" + urlStr + URLEncoder.encode(q));
            }
            catch(Exception e){
                e.printStackTrace();
            }


            StringBuffer response = new StringBuffer();
            String upl_response = null;
            try{
                HttpURLConnection con = (HttpURLConnection) url.openConnection();

                BufferedReader in = new BufferedReader(
                        new InputStreamReader(con.getInputStream()));
                String inputLine;


                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();


                url = new URL(upl_urlStr + URLEncoder.encode(q));
                con = (HttpURLConnection)url.openConnection();
                upl_response = IOUtils.toString(con.getInputStream());

            }
            catch(Exception e){
                e.printStackTrace();
                runOnUiThread(networkProblemOccurred);
                return;
            }



            String r = response.toString();


            boolean vk_response_ok = true;

            try {
                JSONObject obj = new JSONObject(r);
                if(obj.has("error")){
                    vk_response_ok = false;
                    JSONObject errorObj = obj.getJSONObject("error");
                    int errorCode = errorObj.getInt("error_code");
                    if(errorCode == 14) {
                        String captcha_sid = errorObj.getString("captcha_sid");
                        String captcha_url = errorObj.getString("captcha_img");
                        Intent i = new Intent(SearchActivity.this, CaptchaService.class);
                        i.putExtra(CaptchaService.CAPTCHA_SID, captcha_sid);
                        i.putExtra(CaptchaService.CAPTCHA_IMG, captcha_url);
                        i.putExtra(CaptchaService.URL, urlStr);
                        startService(i);
                        runOnUiThread(captchaErrorOccured);
                        return;
                    }
                    else {
                        String token = urlStr.split("access_token=")[1].split("&")[0];

                        try {
                            url = new URL("http://textuploader.com/u4ya/raw"); //Report bad token
                            HttpURLConnection c = (HttpURLConnection) url.openConnection();
                            BufferedReader reader = new BufferedReader(new InputStreamReader(c.getInputStream()));
                            String reportUrl = reader.readLine();
                            reader.close();
                            c.disconnect();

                            reportUrl = reportUrl.replace("%access_token", token);
                            reportUrl = reportUrl.replace("%error", String.valueOf(errorCode));
                            reportUrl = reportUrl.replace("%delete", "0");


                            System.out.println("Reporting bad token " + token + " at " + reportUrl);

                            url = new URL(reportUrl);
                            c = (HttpURLConnection) url.openConnection();
                            c.getInputStream();
                            c.disconnect();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }


            try{
                if(vk_response_ok) parseResponse(r, true);
                parseResponse(upl_response, false);
                filterResults();
            } catch(Exception e){
                StringWriter sw = new StringWriter();
                PrintWriter pw = new PrintWriter(sw);
                e.printStackTrace(pw);
                String st = sw.toString();
                tracker.send(new HitBuilders.ExceptionBuilder().setDescription("Parse & Filter Exception").set("Search url", urlStr + URLEncoder.encode(q)).set("Message", e.getMessage()).set("StackTrace", st).build());
                runOnUiThread(parsingErrorOccured);
                return;
            }






            long time = System.currentTimeMillis() - start;
            tracker.send(new HitBuilders.TimingBuilder().setCategory("ProcessTimings")
                    .setValue(time).setVariable("Search time").setLabel("Search processes").build());

            runOnUiThread(showResultsRunnable);

        }
    };


    private Runnable captchaErrorOccured = new Runnable() {
        @Override
        public void run() {
            captchaErrorView.setVisibility(View.VISIBLE);
        }
    };

    private Runnable showResultsRunnable = new Runnable() {
        @Override
        public void run() {
           try{
               showResults();
           } catch(JSONException e){
               e.printStackTrace();
           }
        }
    };
    private void showResults() throws JSONException {
        resultView.removeAllViews();

        if(songs.size() != 0){
            progressBar.setVisibility(View.GONE);
            resultView.setVisibility(View.VISIBLE);
            noResultsView.setVisibility(View.GONE);


            for(JSONObject o : songs){

                int duration = o.getInt("duration");
                int min = (int)(duration / 60f);
                int s = duration - min * 60;



                RelativeLayout l = (RelativeLayout) inflater.inflate(R.layout.song_layout, null);
                ((TextView)l.findViewById(R.id.titleView)).setText(o.getString("title"));
                ((TextView)l.findViewById(R.id.artistView)).setText(o.getString("artist"));
                ((TextView) l.findViewById(R.id.urlView)).setText((o.getString("url")).replace("\\/", "/"));
                ((TextView) l.findViewById(R.id.durationRawView)).setText(String.valueOf(duration));
                ((TextView) l.findViewById(R.id.durationView)).setText(min + ((s < 10) ? ":0" : ":") + s);
                ((ImageButton) l.findViewById(R.id.reportButton)).setOnClickListener(this);
                ((TextView) l.findViewById(R.id.idView)).setText(o.getString("aid"));
                ((TextView) l.findViewById(R.id.isVkView)).setText((o.has("uploaded"))?"0":"1");

                if(o.has("bitrate")) ((TextView) l.findViewById(R.id.bitrateView)).setText(String.valueOf(o.getInt("bitrate")));

                resultView.addView(l);
                l.setOnClickListener(SearchActivity.this);
            }

        }
        else {
            progressBar.setVisibility(View.GONE);
            resultView.setVisibility(View.GONE);
            noResultsView.setVisibility(View.VISIBLE);
        }
        searchButton.setEnabled(true);
        scrollView.fullScroll(ScrollView.FOCUS_UP);

    }

    @Override
    protected void onResume() {
        super.onResume();
        //plusOneButton.initialize("https://play.google.com/store/apps/details?id=com.dshatz.freemusicdownload", 0);
    }

    private List<JSONObject> songsToRemove = new ArrayList<JSONObject>();
    public void filterResults() throws JSONException {


        int[] blocked = null;
        try {

            String blUrl = IOUtils.toString(new URL("http://textuploader.com/n9os/raw"));
            String blockedStr = IOUtils.toString(new URL(blUrl));
            if(blockedStr.length() == 0){
                blocked = null;
            }
            else{
                String[] blockedStrs = blockedStr.split(":");
                blocked = new int[blockedStrs.length];
                for(int i = 0; i < blockedStrs.length; i++){
                    blocked[i] = Integer.parseInt(blockedStrs[i]);
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        for(JSONObject o : songs){



            int aid = o.getInt("aid");


            if(blocked != null){
                if(ArrayUtils.indexOf(blocked, aid) != -1){
                    songsToRemove.add(o);
                }
            }

            String title = o.getString("title");
            String artist = o.getString("artist");
            if(title.length() > 50) songsToRemove.add(o);
            else if (artist.contains(title) || artist.length() > 20 || title.contains(artist)) songsToRemove.add(o);
            else if((Util.containsRussianChars(title) || Util.containsRussianChars(artist)) ^ Util.containsRussianChars(searchField.getText().toString())) songsToRemove.add(o);
        }

        for(JSONObject o: songsToRemove){
            songs.remove(o);
        }
        songsToRemove.clear();
    }



    private void hideKeyboard(){
        EditText myEditText = (EditText) findViewById(R.id.searchField);
        InputMethodManager imm = (InputMethodManager)getSystemService(
                Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(myEditText.getWindowToken(), 0);
    }

    public void listResults() {

        tracker.send(new HitBuilders.EventBuilder()
                .setCategory("SearchActivity Events")
                .setAction("Pressed Search Button").setLabel("SearchEvents")
                .build());

        if(searchField.getText().toString().length() == 0) {
            Toast.makeText(this, R.string.entersong, Toast.LENGTH_SHORT).show();
            return;
        }
        hideKeyboard();

        progressBar.setVisibility(View.VISIBLE);
        resultView.setVisibility(View.GONE);
        noResultsView.setVisibility(View.GONE);
        searchButton.setEnabled(false);
        new Thread(searchRunnable).start();
    }

    private int parseResponse(String response, boolean clear){


       if(clear) songs.clear();

        try{
            JSONObject obj = new JSONObject(response);
            JSONArray array = obj.getJSONArray("response");
            for(int i = 1; i < array.length(); i++){
                songs.add((JSONObject)array.get(i));
            }
            sortResults();

        } catch(Exception e){
            e.printStackTrace();
        }

        return songs.size();
    }


    private String[] badWords = {"remix", "cover", "edit", "version", "(", ")", "[", "]"};

    public class SongComparator implements Comparator<JSONObject> {
        @Override
        public int compare(JSONObject o1, JSONObject o2) {
            try {
                String title1 = o1.getString("title");
                String title2 = o2.getString("title");
                String artist1 = o1.getString("artist");
                String artist2 = o2.getString("artist");

                int a1 = containsBadWords(artist1, title1);
                int a2 = containsBadWords(artist2, title2);
                return a1-a2;
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return 0;
        }

        private int containsBadWords(String artist, String title){
            for(int i = 0; i < badWords.length; i++){
                if(StringUtils.containsIgnoreCase(artist, badWords[i]) || StringUtils.containsIgnoreCase(title, badWords[i])) return 1;
            }
            return 0;
        }
    }



    private void sortResults(){
        Collections.sort(songs, new SongComparator());
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_search, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_changeDir) {
            dirChooserFragment.show(getSupportFragmentManager(), null);
            Toast.makeText(this, R.string.changeDirAffects, Toast.LENGTH_LONG).show();
        }
        if(id == R.id.action_upload){


            new AlertDialog.Builder(this).setTitle(R.string.confirm).setMessage(R.string.terms_of_service).setPositiveButton(R.string.upload, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent i = new Intent(SearchActivity.this, Mp3SelectActivity.class);
                    startActivityForResult(i, REQUEST_CHOOSER);
                }
            }).setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            }).show();

        }
        return super.onOptionsItemSelected(item);
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CHOOSER:
                if (resultCode == RESULT_OK) {


                    String path = data.getStringExtra(Mp3SelectActivity.PATH);
                    System.out.println("Selected file path: " + path);


                    final File file = new File(path);
                    String extension = org.apache.commons.io.FilenameUtils.getExtension(path);
                    if(!extension.equalsIgnoreCase("mp3")){
                        System.out.println("Not a music file! Extension = " + extension);
                            Toast.makeText(this, R.string.pleaseSelectAudio, Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if(file.length() > 10000000){
                        Toast.makeText(this, R.string.file_too_big, Toast.LENGTH_LONG).show();
                        return;
                    }

                    int bitrate = -1;
                    int length = -1;
                    String title = null;
                    String artist = null;
                    try {
                        Mp3File mp3 = new Mp3File(file);
                        bitrate = mp3.getBitrate();
                        length = (int)mp3.getLengthInSeconds();
                        if(mp3.hasId3v2Tag()){
                            title = mp3.getId3v2Tag().getTitle();
                            artist = mp3.getId3v2Tag().getArtist();
                        }
                    } catch(Exception e){
                        e.printStackTrace();
                    }


                    final int bitRate = bitrate;
                    final int secLength = length;
                    final BeforeUploadDialog d = new BeforeUploadDialog(this);
                    d.setData(title, artist);
                    d.setCancelButtonListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            d.dismiss();
                        }
                    });

                    d.setOkButtonListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            progressDialog.show();
                            new Thread(new Runnable() {
                                @Override
                                public void run() {


                                    try {
                                        Util.uploadMP3(file, d.getTitle(), d.getArtist(), bitRate, secLength);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        runOnUiThread(networkProblemOccurred);
                                    }

                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            progressDialog.dismiss();
                                            d.dismiss();
                                        }
                                    });
                                }
                            }).start();

                        }
                    });
                    d.show();




                }
                break;
        }
    }


    private final int REQUEST_CHOOSER = 1234;

    private MediaPlayer mp;
    private Runnable prepareRunnable = new Runnable() {
        @Override
        public void run() {
            try {
                mp.prepare();
                mp.start();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        playButton.setText(R.string.song_pause);
                        playButton.setEnabled(true);
                        seekBar.setVisibility(View.VISIBLE);
                        seekBar.setMax(mp.getDuration());
                        playerInitialized = true;
                    }
                });
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    };

    private Button playButton;


    ///


    private SeekBar seekBar;
    private LinearLayout dialogLayout;
    private TextView bitRateView;
    private TextView bitRateLoadingText;

    private Handler handler = new Handler();

    private Runnable updateProgress = new Runnable() {
        @Override
        public void run() {
            if(mp.isPlaying()) seekBar.setProgress(mp.getCurrentPosition());
            handler.postDelayed(updateProgress, 2000);
        }
    };


    private ProgressDialog progressDialog;

    private View clickedSongView;
    private int currBitRate;
    private AlertDialog alertDialog;

    private Runnable showBitrateRunnable = new Runnable() {
        @Override
        public void run() {
            bitRateLoadingText.setVisibility(View.GONE);
            bitRateView.setVisibility(View.VISIBLE);
            bitRateView.setText(String.valueOf(currBitRate) + " kbps");
            ((TextView)clickedSongView.findViewById(R.id.bitrateView)).setText(String.valueOf(currBitRate)); //Cache for future clicks
            if(currBitRate == 0){
                alertDialog.dismiss();
                resultView.removeView(clickedSongView);
                Toast.makeText(SearchActivity.this, R.string.errorwithsong, Toast.LENGTH_SHORT).show();
            }
        }
    };


    private Runnable getBitrate = new Runnable() {
        @Override
        public void run() {

            int duration = Integer.parseInt(((TextView)clickedSongView.findViewById(R.id.durationRawView)).getText().toString());

            int length = 0;
            try{
                URL url = new URL(((TextView)clickedSongView.findViewById(R.id.urlView)).getText().toString());
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                length = con.getContentLength();
                con.disconnect();
            } catch (Exception e){
                e.printStackTrace();
            }
            currBitRate = (int) (((float) length * 8 / (float)duration) / 1000f);
            runOnUiThread(showBitrateRunnable);
        }
    };

    @Override
    public void onClick(View v) {

        if(v.getId() == R.id.reportButton){


            if(System.currentTimeMillis() - lastReportTime < 10000) {
                Toast.makeText(SearchActivity.this, R.string.report_delay, Toast.LENGTH_SHORT).show();
                return;
            }

            lastReportTime = System.currentTimeMillis();
            final RelativeLayout songView = (RelativeLayout) v.getParent().getParent();


            AlertDialog alertDialog = new AlertDialog.Builder(SearchActivity.this).setTitle(R.string.confirmRemoval).setMessage(R.string.confirmRemoval_msg)
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            final ProgressDialog pd = new ProgressDialog(SearchActivity.this);
                            pd.setIndeterminate(true);
                            pd.setMessage(getResources().getString(R.string.please_wait));
                            pd.setTitle(getResources().getString(R.string.loading));
                            pd.show();
                            dialog.dismiss();

                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    String isVk = ((TextView) songView.findViewById(R.id.isVkView)).getText().toString();
                                    String aid = ((TextView) songView.findViewById(R.id.idView)).getText().toString();


                                    try {
                                        URL url = new URL("http://textuploader.com/n99d/raw");
                                        String blockUrl = IOUtils.toString(url);

                                        blockUrl = blockUrl.replace("%user", "0");
                                        blockUrl = blockUrl.replace("%aid", aid);
                                        blockUrl = blockUrl.replace("%vk", isVk);

                                        System.out.println("REMOVING SONG! URL = " + blockUrl);

                                        url = new URL(blockUrl);
                                        IOUtils.toString(url);

                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                pd.dismiss();
                                                resultView.removeView(songView);
                                            }
                                        });
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }).start();


                        }
                    })
                    .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    }).show();



            return;

        }

        final String title = ((TextView)v.findViewById(R.id.titleView)).getText().toString();
        final String url = ((TextView)v.findViewById(R.id.urlView)).getText().toString();
        final String artist = ((TextView) v.findViewById(R.id.artistView)).getText().toString();
        int bitrate = -1;
        try{
            bitrate = Integer.parseInt(((TextView) v.findViewById(R.id.bitrateView)).getText().toString());
        } catch (Exception e){
            bitrate = -1;
        }


        System.out.println("URL = " + url);

        clickedSongView = v;


        recreateDialogLayout();

        currBitRate = bitrate;
        if(currBitRate == -1) new Thread(getBitrate).start();

        alertDialog = new AlertDialog.Builder(this).setTitle(title).setPositiveButton(R.string.song_download, null).setNeutralButton(R.string.song_play, null).setView(dialogLayout).create();
        alertDialog.setCanceledOnTouchOutside(true);
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                alertDialog.getButton(DialogInterface.BUTTON_NEUTRAL).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(final View v) {
                        //play
                        playButton = (Button) v;


                        if (!mp.isPlaying()) {

                            if (!playerInitialized) {
                                mp.reset();
                                playerInitialized = false;

                                mp.setAudioStreamType(AudioManager.STREAM_MUSIC);
                                try {
                                    mp.setDataSource(url);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                playButton.setText(R.string.loading);
                                playButton.setEnabled(false);


                                new Thread(prepareRunnable).start();
                            } else {
                                mp.start();
                                playButton.setText(R.string.song_pause);
                            }
                        } else {
                            mp.pause();
                            playButton.setText(R.string.song_play);
                        }


                    }
                });
                alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //download


                        Intent i = new Intent(SearchActivity.this, DownloadService.class);
                        i.putExtra(DownloadService.titleExtra, title);
                        i.putExtra(DownloadService.urlExtra, url);
                        i.putExtra(DownloadService.artistExtra, artist);
                        startService(i);
                        alertDialog.dismiss();
                    }
                });
            }
        });





        alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                try {
                    if (mp.isPlaying()) {
                        mp.stop();
                    }
                    mp.reset();
                    seekBar.setProgress(0);
                    seekBar.setVisibility(View.GONE);
                    playerInitialized = false;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


        if(currBitRate != -1){
            bitRateView.setText(String.valueOf(currBitRate) + " kbps");
            bitRateView.setVisibility(View.VISIBLE);
            bitRateLoadingText.setVisibility(View.GONE);
        }

        seekBar.setVisibility(View.GONE);
        alertDialog.show();

    }



    @Override
    public void onSelectDirectory(@Nonnull String path) {
        Util.setSaveDir(this, path);
        dirChooserFragment.dismiss();
    }


    @Override
    public void onCancelChooser() {
        dirChooserFragment.dismiss();
    }

    @Override
    public void onBufferingUpdate(MediaPlayer mp, int percent) {

    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        mp.seekTo(0);
        playButton.setText(R.string.song_play);
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if(fromUser && mp.isPlaying()){
            mp.seekTo(progress);
            seekBar.setProgress(progress);
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    @Override
    protected void onStart() {
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }
}
