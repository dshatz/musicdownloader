package com.dshatz.bestmusicdownloader;

/**
 * Created by Danik on 20/05/2015.
 */
public class SearchResult {

    String title, artist, url;
    int bitrate,duration;

    public SearchResult(String title, String artist, String url, int duration, int bitrate){
        this.title = title;
        this.artist = artist;
        this.url = url;
        this.duration = duration;
        this.bitrate = bitrate;
    }

}
