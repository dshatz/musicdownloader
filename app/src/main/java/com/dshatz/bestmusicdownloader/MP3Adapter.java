package com.dshatz.bestmusicdownloader;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Danik on 20/05/2015.
 */


public class MP3Adapter extends ArrayAdapter<MP3> {
    public MP3Adapter(Context context, ArrayList<MP3> users) {
        super(context, 0, users);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        MP3 mp3 = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.mp3_list_element, parent, false);
        }

        TextView titleView = (TextView) convertView.findViewById(R.id.title);
        TextView artistView = (TextView) convertView.findViewById(R.id.artist);


        titleView.setText(mp3.title);
        artistView.setText(mp3.artist);


        return convertView;
    }
}