# README #
This is an Android application which is able to download MP3 music for free from [http://vk.com](Link URL) song database. The main advantage of this application is that it doesn't require VK authorization. This makes MusicDownloader usable for people without VK account and from all over the world.

How does this work?
 Secretly, there is another Android application published in Google Play, which is designed for people with VK account. Here it is: [https://bitbucket.org/dshatz/vkmusicdownloader](Link URL). That application also lets users download music from VK database, except they have to login and provide the application VK API *access token*. This access token is then sent to the server, where it is inserted into the database. Tokens from this database are then used in MusicDownloader, which results in a fully working structure.

Reached over 200,000 downloads on Google Play, mostly the U.S. residents.

As the download count was growing, copyright holders found both applications and soon it was banned from Google Play forever. Since then, the application development have been abandoned.